public class MeanMode {
    public static void main(String args[]) {
        int[] input = new int[]{5, 3, 3, 3, 1};
        
        MeanMode(input);
    }
    
    public static void MeanMode(int[] input)
    {
        int mean = 0;
        for(int i=0; i<input.length; i++)
        {
            mean += input[i];
        }
        mean = mean/input.length;
        
        int mode = 0;
        int modeOccurance = 0;
        for(int i=0; i<input.length; i++)
        {
            int modeCandidate = input[i];
            int modeOccuranceCounter = 0;
            for(int x=0; x<input.length; x++)
            {
                if(modeCandidate == input[x]) modeOccuranceCounter++;
            }
            
            if(modeOccuranceCounter > modeOccurance) 
            {
                mode = modeCandidate;
                modeOccurance = modeOccuranceCounter;
            }
        }
        
        if(mean == mode) System.out.println(1);
        else System.out.println(0);
    }
}
