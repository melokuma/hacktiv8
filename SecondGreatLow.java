public class SecondGreatLow {
    public static void main(String args[]) {
        int[] input = new int[]{7,7,12,98,100};
        
        SecondGreatLow(input);
    }
    
    public static void SecondGreatLow(int[] input)
    {
        if(input.length <= 1) 
        {
            System.out.println("Invalid input");
            return;
        }
        
        if(input.length == 2)
        {
            System.out.println(input[0] + " " + input [1]);
            return;
        }
        
        int secondLow = 0;
        int secondLowCounter = 0;
        int secondHigh = 0;
        int secondHighCounter = 0;
        
        for(int i=0; i<input.length; i++)
        {
            if(input[i] > secondLow && secondLowCounter < 2)
            {
                secondLow = input[i];
                secondLowCounter++;
            }
            
            if(input[i] > secondHigh && i < input.length - 1) secondHigh = input[i];
        }
        
        System.out.println(secondLow + " " + secondHigh);
    }
}
