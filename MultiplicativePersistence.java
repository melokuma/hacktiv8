public class MultiplicativePersistence {
    public static void main(String args[]) {
        int input = 39;
        
        MultiplicativePersistence(input);
    }
    
    public static void MultiplicativePersistence(int input)
    {
        boolean result = false;
        
        int processCycling = 0;
        
        while(result == false)
        {
            char[] chars = ("" + input).toCharArray();
            int multiplicationResult = 1;
            for(int i=0; i<chars.length; i++)
            {
                multiplicationResult = multiplicationResult * Character.getNumericValue(chars[i]);
            }
            
            int resultDigit = String.valueOf(multiplicationResult).length();
            
            processCycling++;
            input = multiplicationResult;
            
            if(resultDigit == 1) 
            {
                result = true;
                System.out.println(processCycling);
            }
        }
    }
}
